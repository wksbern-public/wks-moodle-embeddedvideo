<?php
// This file is part of the WKS Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Embedded video view renderer
 *
 * @package mod
 * @subpackage embeddedvideo
 * @author Liip <https://www.liip.ch/>
 * @author Pascal Cudré <pascal.cudre@liip.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_embeddedvideo\output;
defined('MOODLE_INTERNAL') || die();


/**
 * Class view_renderer
 */
class view_renderer extends \plugin_renderer_base {

    public function view_page(\stdClass $cm, \stdClass $embeddedvideo, $embeddedcode) {
        $data = new \stdClass();

        $data->description = format_module_intro('embeddedvideo', $embeddedvideo, $cm->id, true);

        if (!empty($embeddedcode)) {
            $data->iframe = html_entity_decode($embeddedcode->html);
        } else {
            print_error('invalidvideo', 'embeddedvideo');
        }

        return $this->render_from_template('mod_embeddedvideo/view', $data);
    }
}
