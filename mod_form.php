<?php
// This file is part of the WKS Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Embedded video form.
 *
 * @package mod
 * @subpackage embeddedvideo
 * @author Liip <https://www.liip.ch/>
 * @author Pascal Cudré <pascal.cudre@liip.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->dirroot.'/mod/embeddedvideo/lib.php');

class mod_embeddedvideo_mod_form extends moodleform_mod {
    protected function definition() {
        global $CFG;

        $mform = $this->_form;

        $mform->addElement('text', 'name', get_string('name'), array('size' => '128'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        // Introduction.
        $this->standard_intro_elements(get_string('moduleintro'));

        $mform->addElement('text', 'videourl', get_string('videourl', 'mod_embeddedvideo'), array('size' => '256'));
        $mform->setType('videourl', PARAM_URL);

        $mform->addElement('select', 'videoplatform', get_string('videoplatform', 'mod_embeddedvideo'), array(
            'youtube' => 'youtube',
            'vimeo' => 'vimeo'
        ), array("size" => 1));
        $mform->setType('videoplatform', PARAM_URL);

        $this->standard_coursemodule_elements();
        $this->add_action_buttons();
    }
}