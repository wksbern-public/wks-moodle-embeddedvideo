<?php
// This file is part of the WKS Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Define backup_embeddedvideo_activity_task class
 *
 * @package mod
 * @subpackage embeddedvideo
 * @category restore
 * @copyright  2016 onwards liip AG {@link http://liip.ch}
 * @author Pascal Cudré <pascal.cudre@liip.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the restore steps that will be used by the restore_embeddedvideo_activity_task
 */

/**
 * Structure step to restore one embeddedvideo activity
 */
class restore_embeddedvideo_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {

        $paths = array();
        $paths[] = new restore_path_element('embeddedvideo', '/activity/embeddedvideo');

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }

    protected function process_embeddedvideo($data) {
        global $DB;

        $data = (object) $data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        // Set default for possible missing values.

        if (empty($data->timecreated)) {
            $data->timecreated = time();
        }

        if (empty($data->timemodified)) {
            $data->timemodified = time();
        }

        // Create the embeddedvideo instance.
        $newitemid = $DB->insert_record('embeddedvideo', $data);
        $this->apply_activity_instance($newitemid);
    }

    /**
     * Post-execution actions
     */
    protected function after_execute() {
        // Add embeddedvideo related files, no need to match by itemname (just internally handled context)
        $this->add_related_files('mod_embeddedvideo', 'intro', null);
    }
}


