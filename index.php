<?php
// This file is part of the WKS Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Embedded video index page
 *
 * @package mod
 * @subpackage embeddedvideo
 * @author Liip <https://www.liip.ch/>
 * @author Pascal Cudré <pascal.cudre@liip.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once('lib.php');

$id = required_param('id', PARAM_INT);
$PAGE->set_url('/mod/embeddedvideo/index.php', array('id' => $id));

// Ensure that the course specified is valid.
if (!$course = $DB->get_record('course', array('id' => $id))) {
    print_error('invalidcourseid');
}
