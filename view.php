<?php
// This file is part of the WKS Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Embedded video view page.
 *
 * @package mod
 * @subpackage embeddedvideo
 * @author Liip <https://www.liip.ch/>
 * @author Pascal Cudré <pascal.cudre@liip.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot . '/mod/embeddedvideo/lib.php');


$id = required_param('id', PARAM_INT); // Course module ID.

if (!$cm = get_coursemodule_from_id('embeddedvideo', $id)) {
    print_error('invalidcoursemodule');
}
if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
    print_error('coursemisconf');
}

if (!$embeddedvideo = $DB->get_record('embeddedvideo', array('id' => $cm->instance))) {
    print_error('invalidvideo', 'embeddedvideo');
}
require_course_login($course, true, $cm);

$context = context_module::instance($cm->id);
require_capability('mod/embeddedvideo:view', $context);

$title = format_string($cm->name);
$url = new moodle_url('/mod/embeddedvideo/view.php', array('id' => $id));
$PAGE->set_url($url);
$PAGE->set_context($context);
$PAGE->set_cm($cm);
$PAGE->set_title("{$COURSE->fullname}: " . $title);
$PAGE->set_heading("{$COURSE->fullname}: " . $title);
$PAGE->set_pagelayout('incourse');

if ($embeddedvideo->videoplatform == 'youtube') {
    $embeddedcode = get_youtube_embedded_video($embeddedvideo->videourl);
} else {
    $embeddedcode = get_vimeo_embedded_video($embeddedvideo->videourl);
}

echo $OUTPUT->header();
echo $OUTPUT->heading(format_string($title), 2);
$renderer = $PAGE->get_renderer('mod_embeddedvideo', 'view');
echo $renderer->view_page($cm, $embeddedvideo, $embeddedcode);

echo $OUTPUT->footer();