<?php
// This file is part of the WKS Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Embedded video activity strings.
 *
 * @package mod
 * @subpackage lang
 * @author Liip <https://www.liip.ch/>
 * @author Pascal Cudré <pascal.cudre@liip.ch>
 * @author Pascal Thormeier <pascal.thormeier@liip.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['modulename'] = 'Eingebettetes Video';
$string['modulenameplural'] = 'Eingebettete Videos';
$string['modulename_help'] = 'Das "eingebettetes Video" Plugin erlaubt es, Videos von YouTube oder Vimeo im Kurs einzufügen';
$string['pluginadministration'] = 'Embedded Video administration';
$string['pluginname'] = 'Eingebettetes Video';
$string['invalidvideo'] = 'Invalid Embedded Video requested'; // TODO: translate this?
$string['videourl'] = 'Video Teilen URL';
$string['videoplatform'] = 'Video-Plattform';
