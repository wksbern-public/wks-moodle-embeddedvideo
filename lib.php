<?php
// This file is part of the WKS Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Embedded video library.
 *
 * @package mod
 * @subpackage embeddedvideo
 * @author Liip <https://www.liip.ch/>
 * @author Pascal Cudré <pascal.cudre@liip.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Define the core features this plugin supports
 *
 * @param string $feature FEATURE_xx constant for requested feature
 * @return bool true if embeddedvideo supports feature
 */
function embeddedvideo_supports($feature) {
    switch($feature) {
        case FEATURE_GROUPS:
            return false;
        case FEATURE_GROUPINGS:
            return false;
        case FEATURE_MOD_INTRO:
            return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS:
            return true;
        case FEATURE_COMPLETION_HAS_RULES:
            return false;
        case FEATURE_GRADE_HAS_GRADE:
            return false;
        case FEATURE_GRADE_OUTCOMES:
            return false;
        case FEATURE_BACKUP_MOODLE2:
            return true;
        case FEATURE_SHOW_DESCRIPTION:
            return true;
        case FEATURE_CONTROLS_GRADE_VISIBILITY:
            return false;
        case FEATURE_USES_QUESTIONS:
            return false;
        default:
            return null;
    }
}

/**
 * Add embeddedvideo instance.
 *
 * @param stdClass $instance
 * @param stdClass $mform
 * @return int new embeddedvideo instance id
 */
function embeddedvideo_add_instance($instance, $mform) {
    global $DB;

    // Add timecreated and timemodified.
    $instance->timecreated = time();
    $instance->timemodified = $instance->timecreated;

    return $DB->insert_record('embeddedvideo', $instance);
}

/**
 * Update embeddedvideo instance.
 *
 * @param stdClass $instance
 * @param stdClass $mform
 * @return bool true
 */
function embeddedvideo_update_instance($instance, $mform) {
    global $DB;

    // Update timemodified and set ID.
    $instance->timemodified = time();
    $instance->id = $instance->instance;

    return $DB->update_record('embeddedvideo', $instance);
}

/**
 * Delete embeddedvideo instance by activity id
 *
 * @param int $id
 * @return bool success
 */
function embeddedvideo_delete_instance($instanceid) {
    global $DB;

    if (!$instance = $DB->get_record('embeddedvideo', array('id' => $instanceid))) {
        return false;
    }

    return $DB->delete_records('embeddedvideo', array('id' => $instance->id));
}

/**
 * @param string $videovimeourl
 * @param int    $width
 *
 * @return stdClass
 */
function get_vimeo_embedded_video($videovimeourl, $width = 640) {
    $parsedUrl = parse_url($videovimeourl);
    $videoId = ltrim($parsedUrl['path'], '/');

    $returnData = new stdClass();

    $returnData->html = '<iframe src="https://player.vimeo.com/video/' . $videoId . '" width="' . $width . '" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';

    return $returnData;
}

/**
 * @param string $videoyoutubeurl
 * @param int    $width
 *
 * @return stdClass
 */
function get_youtube_embedded_video($videoyoutubeurl, $width = 640) {
    $parsedUrl = parse_url($videoyoutubeurl);
    $videoId = ltrim($parsedUrl['path'], '/');

    $returnData = new stdClass();

    $returnData->html = '<iframe width="' . $width . '" height="315" src="https://www.youtube.com/embed/' . $videoId . '" frameborder="0" allowfullscreen></iframe>';

    return $returnData;
}

/**
 * Helper for getting the JSON.
 * @param $url
 * @return mixed
 */
function curl_get($url) {
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    $return = curl_exec($curl);
    curl_close($curl);
    return $return;
}
